# Available Hotels

## Summary:

This project is a **RESTful API** to get the available hotels from different providers based on the city, check-in date, check-out date and number of guests. In this doc you will find all the technical details and how to maintain the project and how to add new providers/features.
  
## Tech stack:
- Ruby v2.5.0
- Ruby on Rails v5.2.1
- SQLite
- Docker

## Project components
- Best Hotels Service (Hotels Provider)
- Crazy Hotels Service (Hotels Provider)
- Available Hotels Service

## Project Structure

Open your **Terminal** and navigate to the project root directory. In the root directory you will find 2 directories:
- providers (Which has the providers services)
 - available_hotels (The Available Hotels Service)

Navigate to the providers directory and you should find the 2 directories:
- best_hotels (The Best Hotels Service)
- crazy_hotels (The Crazy Hotels Service)

## How to run

 Make sure that you have **Docker** and **Docker Compose** installed on your OS. Open your **Terminal** and navigate to any of the provider services directories and execute the following

If you want to work with **Docker** just navigate to the project dir in your **terminal** and execute the following :

```sh
docker-compose up
```

For the **Best Hotels Service** it will run on port **3001** and the **Crazy Hotels Service** will run on port **3002**. Now navigate to the **Available Hotels Service** directory and execute the same command to start the service which will run on port **3000**.

Now in your browser try the following:

- Best Hotels Service: [http://localhost:3001/api/v1/hotels?city=DUBAI&fromDate=2018-10-17T08:05:23.653Z&toDate=2018-10-19T08:05:23.653Z&numberOfAdults=1](http://localhost:3001/api/v1/hotels?city=DUBAI&fromDate=2018-10-17T08:05:23.653Z&toDate=2018-10-19T08:05:23.653Z&numberOfAdults=1)
- Crazy Hotels Service:  [http://localhost:3002/api/v1/hotels?city=ALEX&from=2018-10-17&to=2018-10-19&adultsCount=1](http://localhost:3002/api/v1/hotels?city=ALEX&from=2018-10-17&to=2018-10-19&adultsCount=1)
- Available Hotels Service: [http://localhost:3000/api/v1/hotels?city=DUBAI&fromDate=2018-10-17&toDate=2018-10-19&numberOfAdults=1](http://localhost:3000/api/v1/hotels?city=DUBAI&fromDate=2018-10-17&toDate=2018-10-19&numberOfAdults=1)
  

## How to run without Docker

Install **ruby 2.5.0** on your system ( I recommend using [RVM](https://rvm.io) ) then navigate to every project dir in your terminal and execute the following :
  
```sh
gem install bundler
bundle install
bundle exec rake db:migrate
bundle exec rake db:seed
```

Then to start the project :

```sh
bundle exec rails s -p {{PORT}} -b '0.0.0.0'
```

And replace **{{PORT}}** with **3001** for the **Best Hotels Service**, **3002** for the **Crazy Hotels Service** and **3000** for **Available Hotels Service**.

## How to add a new provider

In **Available Hotels Service** you have a parent class **HotelsProvider** which you have to extend it in any new provider. So create a new ruby file under **app/providers** for example **my_provider.rb** and extend from **HotelsProvider** class :

```ruby
# app/providers/my_provider.rb
class  MyProvider < HotelsProvider

end
```

And in the class initializer you have to define some configurations for this new provider like :

- base_url
- path_url
- prvider_name

For Example :

```Ruby
# app/providers/my_provider.rb
class  MyProvider < HotelsProvider
  def
    @base_url  =  'http://www.my-provider.com'
    @path_url  =  '/api/v1/hotels'
    @from_date_key  =  'fromDate'
    @to_date_key  =  'toDate'
    @city_key  =  'city'
    @number_of_adults_key  =  'adultsCount'
    @prvider_name  =  'My Provider'
  end
end
```

And now is the time to implement the **hotels** method which will hit on the new provider **API** to get the list of hotels. In this method we will get the **JSON** response as **String** then we'll parse to **JSON Array** and we'll map it to an **Array** of **Hotel** objects :

```Ruby
# app/providers/my_provider.rb
class  MyProvider < HotelsProvider
  def initialize
    @base_url  =  'http://www.my-provider.com'
    @path_url  =  '/api/v1/hotels'
    @from_date_key  =  'fromDate'
    @to_date_key  =  'toDate'
    @city_key  =  'city'
    @number_of_adults_key  =  'adultsCount'
    @prvider_name  =  'My Provider'
  end

  def hotels(from_date, to_date, city, number_of_adults)
    # Prepare the full URL
    url  =  @base_url  +  @path_url
    # GET request on the API with the required params
    response  =  RestClient.get url, params: { @from_date_key => from_date, @to_date_key => to_date, @city_key => city, @number_of_adults_key => number_of_adults }
    # Parsing the response to JSON Object
    hotels_json  =  JSON.parse(response.body)
    hotels_array  = []
    hotels_json.each do |hotel|
      # Create a new Object of Hotel app/models/hotel.rb
      # And mapping the JSON data to it
      hotel_model  =  Hotel.new
      hotel_model.provider  =  @prvider_name
      hotel_model.hotelName  = hotel['hotelName']
      hotel_model.fare  = hotel['price']
      hotel_model.amenities  = hotel['amenities']
      # Append the new Hotel Object to the Hotels Array
      hotels_array.append(hotel_model)
    end
    # Return the Hotels Array
    hotels_array
  # Handling the JSON Parsing Exception and some HTTP/TCP Exceptions  
  rescue  Timeout::Error, Errno::EINVAL, Errno::ECONNRESET, Errno::ECONNREFUSED,Net::HTTPBadResponse, Net::HTTPHeaderSyntaxError,Net::ProtocolError, RestClient::ExceptionWithResponse => e
    # Send the Exception to the ExceptionHandler to notify the admin
    ExceptionHandler.report_error e
    # Return an empty array
    []
  end
end
```

Now you **Provider** is ready to be integrated in the the **Available Hotels Service**. Open **app/controllers/api/v1/hotels_controller.rb**  and inside the **index** method create a new object of your new **provider** then call the **hotels** method with the params and merge the output with the **hotels** array :

```Ruby
my_provider = MyProvider.new
my_provider_result = my_provider.hotels(params[:fromDate],params[:toDate],params[:city],params[:numberOfAdults])
hotels  =  best_hotels_result  + crazy_hotels_result + my_provider_result
```

Now your new **provider**  will be part of the **Available Hotels Service**.