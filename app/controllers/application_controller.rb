class ApplicationController < ActionController::API
  class MethodNotImplemented < StandardError; end
end
