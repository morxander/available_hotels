# The Hotels API Controller
module Api::V1
  class HotelsController < ApplicationController
    # Validate the requires params before the index action
    before_action :validate_params, only: [:index]

    # GET /api/v1/hotels
    # @param [String] city
    # @param [ISO_LOCAL_DATE] fromDate
    # @param [ISO_LOCAL_DATE] toDate
    # @param [Integer] numberOfAdults
    def index
      # Getting all the hotels after sanitizing the user params
      best_hotels_provider = BestHotelsProvider.new
      best_hotels_result = best_hotels_provider.hotels(params[:fromDate],
                                                       params[:toDate],
                                                       params[:city],
                                                       params[:numberOfAdults])
      crazy_hotels_provider = CrazyHotelsProvider.new
      crazy_hotels_result = crazy_hotels_provider.hotels(params[:fromDate],
                                                         params[:toDate],
                                                         params[:city],
                                                         params[:numberOfAdults])
      hotels = best_hotels_result + crazy_hotels_result
      # Render the result as JSON
      render json: hotels
    end

    # The params validator method
    # TODO: Validate the date params
    def validate_params
      if params[:city].blank? || \
         params[:fromDate].blank? || \
         params[:toDate].blank? || \
         params[:numberOfAdults].blank?
        render json: { error: 'Please send the required params' }, status: 400
      end
    end
  end
end
