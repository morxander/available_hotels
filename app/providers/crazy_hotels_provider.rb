# Crazy Hotels Provider
class CrazyHotelsProvider < HotelsProvider
  def initialize
    @base_url = 'http://localhost:3002'
    @path_url = '/api/v1/hotels'
    @from_date_key = 'from'
    @to_date_key = 'to'
    @city_key = 'city'
    @number_of_adults_key = 'adultsCount'
    @prvider_name = 'Crazy Hotels'
  end

  def hotels(from_date, to_date, city, number_of_adults)
    url = @base_url + @path_url
    response = RestClient.get url,
                              params: { @from_date_key => from_date,
                                        @to_date_key => to_date,
                                        @city_key => city,
                                        @number_of_adults_key => number_of_adults }
    hotels_json = JSON.parse(response.body)
    hotels_array = []
    hotels_json.each do |hotel|
      hotel_model = Hotel.new
      hotel_model.provider = @prvider_name
      hotel_model.hotelName = hotel['hotelName']
      hotel_model.fare = hotel['price']
      hotel_model.amenities = hotel['amenities']
      hotels_array.append(hotel_model)
    end
    hotels_array
  rescue Timeout::Error, Errno::EINVAL, Errno::ECONNRESET, Errno::ECONNREFUSED,
         Net::HTTPBadResponse, Net::HTTPHeaderSyntaxError,
         Net::ProtocolError, RestClient::ExceptionWithResponse => e
    ExceptionHandler.report_error e
    []
  end
end
