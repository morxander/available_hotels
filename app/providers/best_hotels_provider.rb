# Best Hotels Provider
class BestHotelsProvider < HotelsProvider
  def initialize
    @base_url = 'http://localhost:3001'
    @path_url = '/api/v1/hotels'
    @from_date_key = 'fromDate'
    @to_date_key = 'toDate'
    @city_key = 'city'
    @number_of_adults_key = 'numberOfAdults'
    @prvider_name = 'Best Hotels'
  end

  def hotels(from_date, to_date, city, number_of_adults)
    url = @base_url + @path_url
    response = RestClient.get url,
                              params: { @from_date_key => from_date,
                                        @to_date_key => to_date,
                                        @city_key => city,
                                        @number_of_adults_key => number_of_adults }
    hotels_json = JSON.parse(response.body)
    hotels_array = []
    hotels_json.each do |hotel|
      hotel_model = Hotel.new
      hotel_model.provider = @prvider_name
      hotel_model.hotelName = hotel['hotel']
      hotel_model.fare = hotel['hotelFare']
      hotel_model.amenities = hotel['roomAmenities'].split(',')
      hotels_array.append(hotel_model)
    end
    hotels_array
  rescue Timeout::Error, Errno::EINVAL, Errno::ECONNRESET, Errno::ECONNREFUSED,
         Net::HTTPBadResponse, Net::HTTPHeaderSyntaxError,
         Net::ProtocolError, RestClient::ExceptionWithResponse => e
    ExceptionHandler.report_error e
    []
  end
end
