# The parrent class of the Hotels providers
class HotelsProvider
  attr_accessor :base_url, :path_url, :from_date_key,
                :to_date_key, :city_key, :number_of_adults_key

  def hotels
    raise ApplicationController::MethodNotImplemented
  end
end
